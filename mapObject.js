function mapObject(obj, cb) {
  try {
    const newObj = {};
    for (let key in obj) {
      if (obj.hasOwnProperty(key)) {
        newObj[key] = cb(obj[key]);
      }
    }
    return newObj;
  } catch (error) {
    console.error("Error is :", error.message);
  }
}

module.exports = mapObject;
