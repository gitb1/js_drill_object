function invert(obj) {
  
 try{ 
    const invertedObj = {};
    for (let key in obj) {
        if (obj.hasOwnProperty(key)) {
            invertedObj[obj[key]] = key;
        }
    }
    return invertedObj;
    }catch(error){
        console.error('Error is:', error.message);
    }
}

module.exports= invert;