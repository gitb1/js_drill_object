function keys(obj) {
 try{
    const result = [];
    for (let key in obj) {
        if (obj.hasOwnProperty(key)) {
            result.push(key);
        }
    }
    return result;
   }catch(error){
    console.error("Error is",error.message);
   }
}

module.exports =keys;
