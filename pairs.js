function pairs(obj) {
  try{
    const pairsArray = [];
    for (let key in obj) {
        if (obj.hasOwnProperty(key)) {
            pairsArray.push([key, obj[key]]);
        }
    }
    return pairsArray;
   }catch(error){
    console.error('Error is :',error.message);
   }
}

module.exports=pairs;